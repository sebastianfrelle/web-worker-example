function prime(n) {
  let primeCount = 0;
  let candidate = 1;
  let p = candidate;

  while (primeCount < n) {
    if (isPrime(candidate)) {
      p = candidate;
      primeCount++;
    }

    candidate++;
  }

  return p;
}

function isPrime(k) {
  if (k < 2) {
    return false;
  }

  let bound = Math.floor(k / 2);

  for (let d = 2; d <= bound; d++) {
    if (k % d === 0) {
      return false;
    }
  }

  return true;
}

// The Web worker part...

onmessage = ({ data: [intent, ...args] }) => {
  switch (intent) {
    case 'prime':
      let [primeN] = args;
      postMessage(prime(primeN));
      break;
    default:
      postMessage(`invalid intent: ${intent}`);
  }
};